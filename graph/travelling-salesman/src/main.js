const GraphVertex = require('./graphVertex');
const GraphEdge = require('./graphEdge');
const Graph = require('./graph');
const travellingSalesman = require('./travellingSalesman');

function generatePermutations(Arr) {
    var permutations = [];
    var A = Arr.slice();

    function swap(a, b) {
        var tmp = A[a];
        A[a] = A[b];
        A[b] = tmp;
    }

    function generate(n, A) {
        if (n == 1) {
            permutations.push(A.slice());
        } else {
            for (var i = 0; i <= n - 1; i++) {
                generate(n - 1, A);
                swap(n % 2 == 0 ? i : 0, n - 1);
            }
        }
    }
    generate(A.length, A);
    return permutations;
}

function generateCityRoutes(cities) {
    let pems = generatePermutations(cities.slice(1));
    for (let i = 0; i < pems.length; i++) {
        pems[i].unshift(cities[0]);
        pems[i].push(cities[0]);
    }
    return pems;
}

//console.log(generatePermutations([1, 2, 3]));
//console.log(generateCityRoutes([0, 1, 2, 3]));


function run() {

    const vertexA = new GraphVertex('A');
    const vertexB = new GraphVertex('B');
    const vertexC = new GraphVertex('C');
    const vertexD = new GraphVertex('D');

    const edgeAB = new GraphEdge(vertexA, vertexB, 1);
    const edgeBD = new GraphEdge(vertexB, vertexD, 1);
    const edgeDC = new GraphEdge(vertexD, vertexC, 1);
    const edgeCA = new GraphEdge(vertexC, vertexA, 1);

    const edgeBA = new GraphEdge(vertexB, vertexA, 5);
    const edgeDB = new GraphEdge(vertexD, vertexB, 8);
    const edgeCD = new GraphEdge(vertexC, vertexD, 7);
    const edgeAC = new GraphEdge(vertexA, vertexC, 4);
    const edgeAD = new GraphEdge(vertexA, vertexD, 2);
    const edgeDA = new GraphEdge(vertexD, vertexA, 3);
    const edgeBC = new GraphEdge(vertexB, vertexC, 3);
    const edgeCB = new GraphEdge(vertexC, vertexB, 9);

    const graph = new Graph(true);

    graph
        .addEdge(edgeAB)
        .addEdge(edgeBD)
        .addEdge(edgeDC)
        .addEdge(edgeCA)
        .addEdge(edgeBA)
        .addEdge(edgeDB)
        .addEdge(edgeCD)
        .addEdge(edgeAC)
        .addEdge(edgeAD)
        .addEdge(edgeDA)
        .addEdge(edgeBC)
        .addEdge(edgeCB);

    const salesmanPath = travellingSalesman(graph);


    console.log(salesmanPath.length);

    console.log(salesmanPath[0].getKey());
    console.log(salesmanPath[1].getKey());
    console.log(salesmanPath[2].getKey());
    console.log(salesmanPath[3].getKey());
}


function minimumSpanningTreeProblem() {

    let mst = {}
    let edges = [20, 30, 40, 50];
    edges.sort();
    let edges = [];

    for (let i of edges) {

        edges.push(i);
    }

}

//minimumSpanningTreeProblem();
run();
