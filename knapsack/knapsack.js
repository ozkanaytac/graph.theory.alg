function knapsack(items, W) {

    let item = 0,
        weight = 0,
        max_before = 0,
        max_after = 0,
        number_of_items = items.length,
        matrix_weight = new Array(number_of_items + 1),
        matrix_to_keep = new Array(number_of_items + 1),
        solution_array = [];

    for (item = 0; item < number_of_items + 1; item++) {
        matrix_weight[item] = new Array(W + 1);
        matrix_to_keep[item] = new Array(W + 1);
    }

    for (item = 0; item <= number_of_items; item++) {
        for (weight = 0; weight <= W; weight++) {
            if (item === 0 || weight === 0) {
                matrix_weight[item][weight] = 0;
            }

            else if (items[item - 1].w <= weight) {
                max_after = items[item - 1].k + matrix_weight[item - 1][weight - items[item - 1].w];
                max_before = matrix_weight[item - 1][weight];

                if (max_after > max_before) {
                    matrix_weight[item][weight] = max_after;
                    matrix_to_keep[item][weight] = 1;
                }
                else {
                    matrix_weight[item][weight] = max_before;
                    matrix_to_keep[item][weight] = 0;
                }
            }
            else {
                matrix_weight[item][weight] = max_before;
                matrix_to_keep[item][weight] = 0;
            }
        }
    }

    weight = W;
    item = number_of_items;
    for (item; item > 0; item--) {
        if (matrix_to_keep[item][weight] === 1) {
            solution_array.push(items[item - 1]);
            weight = weight - items[item - 1].w
        }
    }

    console.log("Max Benefit: ", matrix_weight[number_of_items][W]);
    console.log("Max Benefit From: ", solution_array);

}

let w = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
let k = [0, 1, 2, 3, 4];


knapsack([{ w: 1, k: 2 }, { w: 3, k: 4 }, { w: 2, k: 3 }, { w: 4, k: 6 }], 6);
