from operator import itemgetter
from itertools import groupby


def get_straight(chunk: list, idx: int, straight: int, dice: set) -> int:
    """Get max straight starting at index idx."""
    if idx == len(chunk) - 1:
        return straight

    item = chunk[idx]

    return max((get_straight(chunk, idx+1, straight+1, dice-{x})
                for x in item[1] if x in dice), default=straight)


def longest_straight_length(dices):
    """Return the length of the longest possible straight using `dices`."""
    integers = []
    for j, dice in enumerate(dices):
        integers.extend((val, j) for val in dice)
    integers.sort(key=itemgetter(0))

    grouped = [(k, [x[1] for x in g])
               for k, g in groupby(integers, itemgetter(0))]
    chunks = []

    # find runs of consecutive numbers
    for k, g in groupby(enumerate(grouped), lambda x: x[0]-x[1][0]):
        g = list(g)
        if len(g) > 1:
            chunks.append(list(map(itemgetter(1), g)))

    chunks.sort(key=len, reverse=True)

    max_straight = 1

    for chunk in chunks:
        if len(chunk) <= max_straight:
            break

        for k in range(len(chunk)-1):
            if len(chunk) - k < max_straight:
                break
            dice = set(range(len(dices)))
            straight = get_straight(chunk, k, 0, dice)
            if straight > max_straight:
                max_straight = straight
    return max_straight


def interactive():
    for i in range(int(input())):
        ndice = int(input())
        dices = [[int(x) for x in input().split()] for _ in range(ndice)]
        print("Case #{}: {}".format(i+1, longest_straight_length(dices)))


def unit_tests():
    test_cases = [
        ([[4, 8, 15, 16, 23, 42],
          [8, 6, 7, 5, 30, 9],
          [1, 2, 3, 4, 55, 6],
          [2, 10, 18, 36, 54, 86]],
         4),
        ([[1, 2, 3, 4, 5, 6],
          [60, 50, 40, 30, 20, 10]],
         1),
        ([[1, 2, 3, 4, 5, 6],
          [1, 2, 3, 4, 5, 6],
          [1, 4, 2, 6, 5, 3]],
         3)
    ]
    for dices, expected_result in test_cases:
        actual_result = longest_straight_length(dices)
        if actual_result != expected_result:
            print(expected_result, actual_result, dices)


if __name__ == "__main__":
    if True:
        unit_tests()
    else:
        interactive()
